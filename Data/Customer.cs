﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
   public class Customer
    {
        [Key, Column(Order = 0)]
        public int ID { get; set; }

        [Required]
        [StringLength(8)]
        public string CIN { get; set; }
        [Required(ErrorMessage = " This field is required !")]
        [StringLength(8, ErrorMessage = "User input max 8")]
        [MinLength(3, ErrorMessage = " Storage min 3")]
        public string Nom { get; set; }
        public string Prenom { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }
    }
}

