﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IUserService
    {
        public Task Ajout(Customer customer);
        public IList<Customer> GetAllCustomer();
        public Task Update(Customer customer);
        public Task<Customer> GetCustomerById(int id);
        public Task Delete(Customer customer);

       // public Task IList<Customer> GetMany(Expression<Func<Customer, bool>> where = null);


        /*
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomer(int id);
        void InsertCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(int id);*/
    }
}
