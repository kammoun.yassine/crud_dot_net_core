﻿using Data;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly IGenericRepository<Customer> genericRepo;
        private readonly IUserRepository customerRepo;

        public UserService(IGenericRepository<Customer> genericRepo, IUserRepository CustomerRepo)
        {
            this.genericRepo = genericRepo;
            customerRepo = CustomerRepo;
        }
        public Task Ajout(Customer customer)
        {
            return genericRepo.InsertAsync(customer);
        }

        public Task Delete(Customer customer)
        {
            return genericRepo.DeleteAsync(customer.ID);
        }

        public IList<Customer> GetAllCustomer()
        {
            return genericRepo.GetAll().ToList();
        }

        public Task<Customer> GetCustomerById(int id)
        {
            return genericRepo.GetByIdAsync(id);
        }

       

        public Task Update(Customer customer)
        {
            return genericRepo.PutAsync(customer.ID, customer);
        }





        /*
        private IUserRepository<Customer> _repository;
        public UserService(IUserRepository<Customer> repository)
        {
            _repository = repository;
        }
        public void DeleteCustomer(int id)
        {
            Customer customer = GetCustomer(id);
            _repository.Remove(customer);
            _repository.SaveChanges();
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            return _repository.GetAll(); 
        }

        public Customer GetCustomer(int id)
        {
            return _repository.Get(id);
        }

        public void InsertCustomer(Customer customer)
        {
            _repository.Insert(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            _repository.Update(customer);
        }*/
    }
}
