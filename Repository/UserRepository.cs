﻿using Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UserRepository: IUserRepository //<T> where T : Customer
    {
        //private readonly ApplicationDbContext _applicationDbContext;
        private readonly ApplicationDbContext dbContext;
        private readonly IGenericRepository<Customer> genericRepoCustomer;

        public UserRepository(ApplicationDbContext dbContext, IGenericRepository<Customer> _GenericRepoCustomer)
        {
            this.dbContext = dbContext;
            genericRepoCustomer = _GenericRepoCustomer;
        }






        // private DbSet<T> entities;
        /* public UserRepository(ApplicationDbContext applicationDbContext)
         {
             _applicationDbContext = applicationDbContext;
             entities = _applicationDbContext.Set<T>();
         }
         public void Delete(T entity)
         {
             if (entity == null)
             {
                 throw new ArgumentNullException("entity");
             }
             entities.Remove(entity);
             _applicationDbContext.SaveChanges();
         }

         public T Get(int Id)
         {
             return entities.SingleOrDefault(c => c.ID == Id);
         }

         public IEnumerable<T> GetAll()
         {
             return entities.AsEnumerable();
         }

         public void Insert(T entity)
         {
             if (entity == null)
             {
                 throw new ArgumentNullException("entity");
             }
             entities.Add(entity);
             _applicationDbContext.SaveChanges();
         }

         public void Remove(T entity)
         {
             if (entity == null)
             {
                 throw new ArgumentNullException("entity");
             }
             entities.Remove(entity);
         }

         public void SaveChanges()
         {
             _applicationDbContext.SaveChanges();
         }

         public void Update(T entity)
         {
             if (entity == null)
             {
                 throw new ArgumentNullException("entity");
             }
             entities.Update(entity);
             _applicationDbContext.SaveChanges();
         }*/
    }
}
