﻿using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IUserService _customerService;

        public CustomerController(IUserService customerService)
        {
            _customerService = customerService;
        }

        // GET: CustomerController
        public ActionResult Index(DateTime startdate, DateTime enddate)
        {
            
            var result = _customerService.GetAllCustomer();
            if (result != null)
            {
                return View(result);
            }
            return BadRequest("No records found");
        }

        // GET: CustomerController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var result = await _customerService.GetCustomerById(id);
            if (result != null)
            {
                return View(result);
            }
            return BadRequest("No records found");
        }

        // GET: CustomerController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CustomerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Customer customer)
        {
            try
            {
                await _customerService.Ajout(customer);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return BadRequest("No records found");
            }
        }

        // GET: CustomerController/Edit/5
        public async Task<IActionResult> Edit1(Customer customer)
        {
            var result = await _customerService.GetCustomerById(customer.ID);
            return View(result);
        }

        // POST: CustomerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Customer customer)
        {
            try
            {
                await _customerService.Update(customer);
                
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return BadRequest("No records found");
            }
        }

        // GET: CustomerController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _customerService.GetCustomerById(id);
            return View(result);
        }

        // POST: CustomerController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Customer customer)
        {
            try
            {
                await _customerService.Delete(customer);
                
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return BadRequest("No records found");
            }
        }
    }
}
